"""
    Version : To be filled as per the version to be released

    template.defs
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Implements the support for the raw data from the Math file.

    Game Details:
    ~~~~~~~~~~~~~
    This template is for 5 reels, 3 rows and 10 winning line.
         
"""

from collections import OrderedDict

info = {
    "num_reels": 5,  # num_reels is the total number of reels.
    "view_size": 3,  # view_size are the rows
    "win_lines": 10,  # Total number of winning combination in the game
    "symbol_pos": [  # symbol_pos shows how the grid will be
        [0, 1, 2, 3, 4],
        [5, 6, 7, 8, 9],
        [10, 11, 12, 13, 14]
    ]
}

# symbols is a list of dictionary containing all the information about each symbol.
symbols = [
    {'symbol_index': 0, 'name': 'Theme_0', 'key': 'T0', 'is_wild': False},
    {'symbol_index': 1, 'name': 'Theme_1', 'key': 'T1', 'is_wild': False},
    {'symbol_index': 2, 'name': 'Theme_2', 'key': 'T2', 'is_wild': False},
    {'symbol_index': 3, 'name': 'Theme_3', 'key': 'T3', 'is_wild': False},
    {'symbol_index': 4, 'name': 'Theme_4', 'key': 'T4', 'is_wild': False},
    {'symbol_index': 5, 'name': 'Theme_5', 'key': 'T5', 'is_wild': False},
    {'symbol_index': 11, 'name': 'Wild', 'key': 'WI', 'is_wild': True},
    {'symbol_index': 12, 'name': 'Scatter', 'key': 'SC', 'is_wild': False},
]

# Reel Sets for the game which includes base game and freespin reel sets
reel_sets = [
    {
        "index": 0,
        "type": "regular",
        "weight": 0,    # weights are the probability for choosing reel-sets
        "reels": [
            [],
            [],
            [],
            [],
            []
        ],
    },

    # for freespins, this reelset is chosen
    {
        "index": 1,
        "type": "freespins",
        "reels": [
            [],
            [],
            [],
            [],
            []
        ],
    },
]

'''
    Winning line combination for earning. win_lines collection represented by row id of grid. For eg. consider a grid
                            0 , 0 , 0 , 0, 0,
                            1 , 1 , 1 , 1, 1,
                            2 , 2 , 2 , 2, 2,
    Now, line 1 (that runs from top row) has offsets 1,1,1,1,1
'''
win_lines = [
    [1, 1, 1, 1, 1],  # 0
    [0, 0, 0, 0, 0],  # 1
    [2, 2, 2, 2, 2],  # 2
    [0, 1, 2, 1, 0],  # 3
    [2, 1, 0, 1, 2],  # 4
    [0, 0, 1, 0, 0],  # 5
    [2, 2, 1, 2, 2],  # 6
    [1, 0, 0, 0, 1],  # 7
    [1, 2, 2, 2, 1],  # 8
    [0, 1, 1, 1, 0],  # 9
]

scatter_wins = OrderedDict(
    [
        ((12, 2), {"payout_factor": 2.0, "prize": {"name": "FreeSpins5", "type": "freespins", "spins": 5}}),

        ((12, 3), {"payout_factor": 5.0, "prize": {"name": "FreeSpins10", "type": "freespins", "spins": 10}}),

        ((12, 4), {"payout_factor": 20.0, "prize": {"name": "FreeSpins15", "type": "freespins", "spins": 15}}),

        ((12, 5), {"payout_factor": 100.0, "prize": {"name": "FreeSpins20", "type": "freespins", "spins": 20}}),
    ]
)

payouts = OrderedDict(
    [
        ((0, 2), 10.0),
        ((0, 3), 50.0),  # T0
        ((0, 4), 100.0),
        ((0, 5), 300.0),

        ((1, 2), 0.0),
        ((1, 3), 50.0),  # T1
        ((1, 4), 100.0),
        ((1, 5), 150.0),

        ((2, 2), 0.0),
        ((2, 3), 50.0),  # T2
        ((2, 4), 100.0),
        ((2, 5), 150.0),

        ((3, 2), 0.0),
        ((3, 3), 20.0),  # T3
        ((3, 4), 80.0),
        ((3, 5), 120.0),

        ((4, 2), 0.0),
        ((4, 3), 20.0),  # T4
        ((4, 4), 80.0),
        ((4, 5), 120.0),

        ((5, 2), 0.0),
        ((5, 3), 10.0),  # T5
        ((5, 4), 50.0),
        ((5, 5), 100.0),

    ]
)
