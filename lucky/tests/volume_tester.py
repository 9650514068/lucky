"""
    Version: To be filled as per the version to be released

    client_name.template.volume_tester
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Implements the Volume Test for RTP verification
"""

import argparse
from money import Money
from OGA import EngineValidateError
from decimal import Decimal

from OGA import SlotVolumeTester

from .. import defs
from ..engine import TemplateEngine


class TemplateVolumeTester(SlotVolumeTester):
    num_of_game_id = 0

    def gameplay(self):
        """
        Play out one gameplay and dump results.
        If you override this method, don't forget to include dump_spin_result_csv if self.per_spin is True.
        """
        action = 'spin'
        self.num_of_game_id += 1

        self.p = self.regular_spin(action=action)

        if self.per_spin:
            dump_method = self.dump_spin_result_csv
        else:
            dump_method = self.dump_map_result

        dump_method(self.p)
        while not self.p.finish:
            next_action = self.p.result.get('triggering_state')['next_action']
            if next_action is not '':
                action = next_action
            else:
                raise EngineValidateError('invalid gamestate')
            self.p = self.e.play({'action': action}, storage=self.storage)
            dump_method(self.p)
        self.storage['gameplay'] = {}

    def dump_map_result(self, p):
        """
        Output STAKE and WIN, plus EVENTs for each win-type
        """
        if type(p.stake) is Decimal and type(p.winning) is Decimal:
            self.dump_kv('STAKE', p.stake)
            self.dump_kv('WIN', p.winning)
        elif type(p.stake) is Money and type(p.winning) is Money:
            self.dump_kv('STAKE', p.stake.amount)
            self.dump_kv('WIN', p.winning.amount)
        action = p.result['request']['action']

        self.dump_event(action + '_win', float(p.result['current_winnings']))

        wins = p.result.get('line_wins', [])
        for win in wins:
            self.dump_event(str(action) + win.get('index', 'unindexed'), float(win['winnings']))

        sc_wins = p.result.get('scatter_wins', [])
        for win in sc_wins:
            self.dump_event(str(action) + '_scatter_win_' + str(win['spins']), float(win['winnings']))

        if action == 'spin':
            if p.result.get('freespin_state'):
                self.dump_event('freespin_hits', 1)

    def dump_spin_result_csv(self, p, game_id=1, user_id=1):

        """dump per-spin result as csv line for compliance testing"""

        # variable init
        result = p.result
        action = result['request']['action']
        curr_win = float(result['current_winnings'])
        total_win = float(result['win_amount'])
        game_stops = ':'.join(map(str, result['stop_list']))
        drawn_symbol_grid = [symbol for row in result['drawn_symbol_grid'] for symbol in row]
        drawn_symbol_grid = ':'.join(map(str, drawn_symbol_grid))
        stake = float(result['stake_due'])
        symbol_grid = [symbol for row in result['symbol_grid'] for symbol in row]
        symbol_grid = ':'.join(map(str, symbol_grid))
        num_of_spin=0
        current_play=0
        freespin_winnings=0

        if action == 'freespin':
            num_of_spin = result['freespin_state']['num_free_spins']
            current_play = result['freespin_state']['current_play']
            freespin_winnings = result['freespin_state']['freespin_winnings']


        data = [game_id,
                stake,
                action,
                game_stops,
                symbol_grid,
                drawn_symbol_grid,
                num_of_spin,
                current_play,
                freespin_winnings,
                curr_win,
                total_win,
                ]

        if self.num_of_game_id == 1:
            one_time_str = 'game_id; stake; action; game_stops; symbol_grid; drawn_symbol_grid;' \
                           ' current_play; level; wild_pos; wild_view; freespin_winnings;' \
                           'curr_win; total_win; bonus_winnings\n'

            self.outfile.write(one_time_str)

        out_str = '{};{};{};{};{};{};{};{};{};{};{};\n'.format(*data)

        self.outfile.write(out_str)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--perspin", action="store_true", help="output compliance-type per-spin csv result")
    parser.add_argument("-n", type=int, help="run N times (otherwise will read lines from stdin)")
    args = parser.parse_args()
    TemplateVolumeTester(engine=TemplateEngine, defs=defs, per_spin=args.perspin)(args.n)
