"""
    Version: 0.1.0

    client.template.tests.performance_test
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Implements the Performance Test for the game
"""

import pstats
import cProfile

from ..engine import TemplateEngine
from .volume_tester import TemplateVolumeTester
from .. import defs

def dump_pt(filename):
    profile.dump_stats('{}.prof'.format(filename))
    stream = open('{}.txt'.format(filename), 'w')
    p = pstats.Stats('{}.prof'.format(filename), stream=stream)
    if 'overall' in filename:
        p.sort_stats('cumulative').print_stats()
    else:
        p.sort_stats('filename').print_stats('rank')


if __name__ == '__main__':
    parameters = {'stakeValues': '0.10 0.20 0.25 0.50 1.00 2.00 2.50'}
    profile = cProfile.Profile()
    profile.run(
        'TemplateVolumeTester(engine=TemplateEngine, defs=defs, parameters=parameters)(10000)')
    dump_pt('performance_test')
    dump_pt('performance_test_overall')